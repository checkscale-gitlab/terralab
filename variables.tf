# Variables for Terraform - GitLab project.
variable "gitlab_token" {
  type = "string"
  default = "gitlab_token_here"
}

# Project Vars
variable "project_name" {
  type = "string"
  default = "TerraLab"
}

variable "project_description" {
  type = "string"
  default = "Created with Terraform!"
}

variable "branch" {
  type = "string"
  default = "master"
}