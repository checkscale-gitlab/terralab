# Configure the GitLab Provider
provider "gitlab" {
  token = "${var.gitlab_token}"
}

# Add a project owned by the user
resource "gitlab_project" "project_name" {
  name = "${var.project_name}"
  description = "${var.project_description}"

  visibility_level = "public"

  default_branch = "master"
  initialize_with_readme = true
}

# Add a variable to the project
resource "gitlab_project_variable" "test_variable" {
  project = "${gitlab_project.project_name.id}"

  variable_type = "env_var"
  key = "TEST_VAR"
  value = "test-value"

  protected = true
  masked = false
}

# Add branch protection
resource "gitlab_branch_protection" "MasterProtection" {
  project = "${gitlab_project.project_name.id}"
  branch = "${var.branch}"
  push_access_level = "maintainer"
  merge_access_level = "maintainer"
}